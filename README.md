#  Daftar API Perpustakaan Kemendikbud [https://perpustakaan.kemdikbud.go.id]

Contoh API, attribut & parameter: 
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/news?pageSize=4&sort=%257B%2522createdAt%2522%253A%2522desc%2522%257D&page=1&isShow=true' \
  -H 'Accept: application/hal+json' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/hal+json' \
  -H 'Host: api-perpustakaan.kemdikbud.go.id' \
  -H 'Origin: http://localhost:3001' \
  -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36' \
  -H 'cache-control: no-cache'
```

##### Page : https://perpustakaan.kemdikbud.go.id
Berita
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/news?pageSize=4&sort=%257B%2522createdAt%2522%253A%2522desc%2522%257D&page=1&isShow=true' \
```
Koleksi
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-query?inst_name=kemdikbud&unit_name=perpustakaan&coll_name=opac&rows=4&=' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/jurnal-kemendikbud

Kategori List
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/jurnal-kemdikbud-instansi?active=true' \
```

Data Grid
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/jurnal?category=b17a902f20a04038bb926a892d5b2eb5&pageSize=8&isShow=true&page=1' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/jurnal-internasional
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/jurnal?category=9865bd8b2aac480ca354e5766a605291&pageSize=8&isShow=true&page=1' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/komunitas

Kategori
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/community-category' \
```

Data Grid / pencarian
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/community?description=jakarta&name=jakarta&pageSize=8&sort=%257B%2522createdAt%2522%253A%2522desc%2522%257D&page=1&is_active=true&is_show=true&category=d7367bdd6a9f462c9ca951cc30286684' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/search-home-result

Kategori: Katalog Perpustakaan Kemendikbud (opac)
keyword : komputer
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-query?rows=5&inst_name=kemdikbud&unit_name=perpustakaan&coll_name=opac&q_field=%2A&q_value=komputer&page=1' \
```
Kategori: Katalog Induk (ucs)
keyword : komputer
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-query?rows=5&inst_name=kemdikbud&unit_name=ucs&q_field=%2A&q_value=komputer&page=1&ucs_institution=on' \
```

Kategori: Repositori Institusi (repo)
keyword : komputer
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-query?rows=5&inst_name=kemdikbud&unit_name=perpustakaan&coll_name=repo&q_field=%2A&q_value=komputer&page=1' \
```

Kategori: Jurnal Elektronik (jurnal)
keyword : komputer
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-query?rows=5&inst_name=kemdikbud&unit_name=jurnal&q_field=%2A&q_value=komputer&page=1' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/koleksi-perpus-kemdikbud
Data Grid dengan pencarian, keyword : komputer & field : title
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-query?page=1&inst_name=kemdikbud&unit_name=perpustakaan&coll_name=opac&rows=8&facet=on&q_field=title&q_value=komputer' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/koleksi-perpus-kemdikbud-detail/kemdikbud-perpustakaan-opac-38591
Detail 
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-opac-detail/kemdikbud-perpustakaan-opac-38591' \
```
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-detail/kemdikbud-perpustakaan-opac-38591' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/ucs
Lokasi
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-ucs-coll-options' \
```
Data Grid dengan pencarian, keyword : komputer & lokasi : Perpustakaan Kemendikbud
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-query?rows=10&inst_name=kemdikbud&unit_name=ucs&coll_name=libdiknas&q_field=%2A&q_value=komputer&page=1&ucs_institution=on&facet=on&facet_field=subject&facet.prefix=bahasa' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/detail-ucs/kemdikbud-ucs-libdiknas-32725
Detail 
```
curl -X GET \
  'https://perpustakaan.kemdikbud.go.id/detail-ucs/kemdikbud-ucs-libdiknas-32725' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/repositori-institusi
Data Grid dengan pencarian, keyword : komputer & field : title
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-query?page=1&inst_name=kemdikbud&unit_name=perpustakaan&coll_name=repo&rows=10&facet=on&q_field=title&q_value=komputer' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/repositori-institusi-detail/kemdikbud-perpustakaan-repo-10007
Detail 
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-detail/kemdikbud-perpustakaan-repo-10007' \
```
##### Page : https://perpustakaan.kemdikbud.go.id/ojs
Lokasi
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-jurnal-coll-options' \
```
Data Grid dengan pencarian, keyword : penelitian & lokasi : Jurnal Teknodik
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-query?rows=10&inst_name=kemdikbud&unit_name=jurnal&coll_name=teknodik&q_field=%2A&q_value=penelitian&page=1&jurnal_institution=on&facet=on&facet_field=subject&facet.prefix=bahasa' \
```

##### Page : https://perpustakaan.kemdikbud.go.id/ojs-detail/kemdikbud-jurnal-teknodik-392
Detail 
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/se-detail/kemdikbud-jurnal-teknodik-392' \
```

# Membership

##### Page : https://perpustakaan.kemdikbud.go.id/register
Register 
```
curl -X POST \
  https://api-perpustakaan.kemdikbud.go.id/api/register \
  -H 'Accept: application/hal+json' \
  -H 'Accept-Encoding: gzip, deflate' \
  -d '{ 
  "username":"perpustest2",
  "email":"perpus-test2@yopmail.com",
  "password": "yayan",
  "password_confirmation":"yayan"
}'
```

##### Page : https://perpustakaan.kemdikbud.go.id/register
Username validation 
```
curl -X GET \
  https://api-perpustakaan.kemdikbud.go.id/api/register-username-validation/yayan \
```

Email validation 
```
curl -X GET \
  https://api-perpustakaan.kemdikbud.go.id/api/register-email-validation/perpus-test2@yopmail.com \
```

Login
```
curl -X POST \
  https://api-perpustakaan.kemdikbud.go.id/oauth \
  -d '{
  "grant_type":"password",
  "client_id":"web", 
  "client_secret":"web", 
  "username":"yayan", 
  "password":"yayan"
}'
```

My Profile (auth req)
```
curl -X GET \
  https://api-perpustakaan.kemdikbud.go.id/api/my-profile \
  -H 'Authorization: Bearer 929b937bac984549996c22363cf3737aee244ee3' \
```

Update Profile (auth req)
```
curl -X PATCH \
  https://api-perpustakaan.kemdikbud.go.id/api/my-profile \
  -H 'Authorization: Bearer 929b937bac984549996c22363cf3737aee244ee3' \
  -d '{
  "email":"yayan.sopiyan@gmail.com",
  "first_name":"Yayan", 
  "last_name":"Sopiyan",
  "slims_member_id": "G00000916",
  "slims_password": "cimanggis",
  "google_account": "",
}'
```

Login ke sistem perpustakaan / Slims (auth req)
```
curl -X POST \
  https://api-perpustakaan.kemdikbud.go.id/api/slims-member-login \
  -H 'Authorization: Bearer 060008ba65bf13bf194e48a8ce74ea565c6afdd0' \
  -d '{
  "member_id":"G00000916",
  "password":"cimanggis"
}'
```

Login ke sistem perpustakaan / Slims (auth req)
```
curl -X POST \
  https://api-perpustakaan.kemdikbud.go.id/api/slims-member-login \
  -H 'Authorization: Bearer 060008ba65bf13bf194e48a8ce74ea565c6afdd0' \
  -d '{
  "member_id":"G00000916",
  "password":"cimanggis"
}'
```
response 
```
{
    "status": "success",
    "message": "login successfully",
    "data": {
        "member_id": "G00000916",
        "member_name": "Hendro Wicaksono",
        "gender": 1,
        "birth_date": "-0001-11-30T00:00:00+0707",
        "membership_expiry_date": "2020-09-26T00:00:00+0700"
    },
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzExMDY5OTIsImp0aSI6IklCNDl6elZRV3BOenY5aVd3Ukc4U242OXU4b01ZRWxIR3BucWppZ2tlKzA9IiwiaXNzIjoiYXBpLXNsaW1zLXBlcnB1cy1rZW1lbmRpa2J1ZC5hdGthbGFiLmNvbSIsIm5iZiI6MTU3MTEwNjk5MiwiZXhwIjoxNTcxMTA3NTkyLCJkYXRhIjp7Im1lbWJlcl9pZCI6IkcwMDAwMDkxNiJ9fQ.ok82hxuK6pYvErTAwCSktqpMu--IvgFcickBNpHBZjU"
}
```

Get Loan List sistem perpustakaan / Slims (auth req)
```
curl -X GET \
  'https://api-perpustakaan.kemdikbud.go.id/api/slims-loan-list?member_id=G00000916&token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzExMDY5OTIsImp0aSI6IklCNDl6elZRV3BOenY5aVd3Ukc4U242OXU4b01ZRWxIR3BucWppZ2tlKzA9IiwiaXNzIjoiYXBpLXNsaW1zLXBlcnB1cy1rZW1lbmRpa2J1ZC5hdGthbGFiLmNvbSIsIm5iZiI6MTU3MTEwNjk5MiwiZXhwIjoxNTcxMTA3NTkyLCJkYXRhIjp7Im1lbWJlcl9pZCI6IkcwMDAwMDkxNiJ9fQ.ok82hxuK6pYvErTAwCSktqpMu--IvgFcickBNpHBZjU' \
  -H 'Authorization: Bearer 060008ba65bf13bf194e48a8ce74ea565c6afdd0' \
```
response 
```
{
    "status": "success",
    "message": "Anda mempunyai peminjaman koleksi koleksi.",
    "member_id": "G00000916",
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NzExMDY5OTIsImp0aSI6IklCNDl6elZRV3BOenY5aVd3Ukc4U242OXU4b01ZRWxIR3BucWppZ2tlKzA9IiwiaXNzIjoiYXBpLXNsaW1zLXBlcnB1cy1rZW1lbmRpa2J1ZC5hdGthbGFiLmNvbSIsIm5iZiI6MTU3MTEwNjk5MiwiZXhwIjoxNTcxMTA3NTkyLCJkYXRhIjp7Im1lbWJlcl9pZCI6IkcwMDAwMDkxNiJ9fQ.ok82hxuK6pYvErTAwCSktqpMu--IvgFcickBNpHBZjU",
    "loan_list": [
        {
            "loan_id": 110006,
            "item_code": "000134441",
            "title": "Youtuber for dummies",
            "loan_date": "2019-10-02T00:00:00+0700",
            "due_date": "2019-10-16T00:00:00+0700"
        }
    ]
}
```

